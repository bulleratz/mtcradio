package greenmesh.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class Theme {
    public interface  Initializer {
        void initialize(Theme theme, int themeId);
    }

    public interface ThemeObserver {
        void colorChanged(Theme theme, String ColorId);
        void drawableChanged(String drawableId, Drawable drawable);
    }

    private static HashMap<Integer, Theme >  _themes = new HashMap<>();

    public static Theme getTheme(Integer id) {
        Theme theme = _themes.get(id);
        if (theme == null) {
            theme = new Theme(id);
        }
        return theme;
    }

    private Theme(int themeId)
    {
        _themeId = themeId;
    }

    public Theme() {
        _themeId = 0;
    }

    private ArrayList<WeakReference<ThemeObserver>> _observers = new ArrayList<>();
    private HashMap<String, Integer> _colors = new HashMap<>();
    private HashMap<String, Drawable> _images = new HashMap<>();
    private int _themeId = 0;

    public void addObserver(ThemeObserver observer) {
        if (observer != null) {
            for (int i=0; i<_observers.size(); i++) {
                ThemeObserver obs = _observers.get(i).get();
                if (obs == null) {
                    _observers.remove(i);
                    i--;
                } else {
                    if (obs == observer)
                        return; //alread observing
                }
            }
            _observers.add(new WeakReference<ThemeObserver>(observer));
        }
    }

    private static final int NOTIFY_COLOR_CHANGED = 0;
    private static final int NOTIFY_DRAWABLE_CHANGED = 1;

    private void notifyObserver(int changedAction, String itemId) {
        for (int i=0; i<_observers.size(); i++) {
            ThemeObserver obs = _observers.get(i).get();
            if (obs == null) {
                _observers.remove(i);
                i--;
            } else {
                switch (changedAction) {
                    case NOTIFY_COLOR_CHANGED:
                        ((ThemeObserver) obs).colorChanged(this, itemId);
                        break;
                    case NOTIFY_DRAWABLE_CHANGED:
                        obs.drawableChanged(itemId, _images.get(itemId));
                        break;
                }
            }
        }
    }

    public int getColor(String id) {
        Integer color = _colors.get(id);
        if (color == null)
            color = 0;
        return color;
    }

    public void setColor(String colorId, int color) {
                _colors.put(colorId, color);
                notifyObserver(NOTIFY_COLOR_CHANGED, colorId);
    }

    public void setDrawable(String bitmapId, Drawable drawable) {
        if (drawable != null) {
            _images.put(bitmapId, drawable);
        }
        else
        {
            if (_images.containsKey(bitmapId))
                _images.remove(bitmapId);
        }
        notifyObserver(NOTIFY_DRAWABLE_CHANGED, bitmapId);
    }

    public Drawable getDrawable(String drawableId) {
        if (_images.containsKey(drawableId)) {
            return _images.get(drawableId);
        }
        return null;
    }

    public int getThemeId() {
        return _themeId;
    }

    public void copy(Theme source) {
        _colors.clear();
        _images.clear();
        if (source !=null) {
            copyColors(source);
            copyImages(source);
        }
    }

    private void copyImages(Theme source) {
        HashMap<String, Drawable> imgs = (HashMap<String, Drawable>)source._images.clone();
        Object[] keys = imgs.keySet().toArray();

        for (int i=0; i<keys.length; i++) {
            String key = (String)keys[i];
            Drawable dr = source.getDrawable(key);
            if (dr != null)
                setDrawable(key, dr.getConstantState().newDrawable());
        }
    }

    private void copyColors(Theme source) {
        HashMap<String, Integer> cols = (HashMap<String, Integer>)source._colors.clone();
        Object[] keys = cols.keySet().toArray();

        for (int i=0; i<keys.length; i++) {
            String key = (String)keys[i];
            setColor(key, source.getColor(key));
        }
    }

    public void reset() {}

    public boolean save(String filename) {
        File file = new File(filename);
        try {
            JSONObject root = new JSONObject();

            JSONObject themeObject = new JSONObject();
            themeObject.put("Name", "");

            JSONArray colors = new JSONArray();
            Object[] keys = _colors.keySet().toArray();
            for (int i=0; i<keys.length; i++) {
                JSONObject colorObject = new JSONObject();
                colorObject.put("Key", keys[i].toString());
                Integer value = _colors.get(keys[i].toString());
                colorObject.put("Value", Integer.toHexString(value));
                colors.put(colorObject);
            }
            themeObject.put("Colors", colors);

            JSONArray images = new JSONArray();
            keys = _images.keySet().toArray();
            for (int i=0; i<keys.length; i++) {
                JSONObject colorObject = new JSONObject();
                colorObject.put("Key", keys[i].toString());
                Drawable value = _images.get(keys[i].toString());
                Bitmap bitmap = ((BitmapDrawable)value).getBitmap();
                if (bitmap.getWidth() > 1024 || bitmap.getHeight() > 600) {
                    bitmap = Bitmap.createScaledBitmap(bitmap, 1024, 600, true);
                }
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] bitmapdata = stream.toByteArray();
                String base64 = new String(Base64.encode(bitmapdata, Base64.CRLF));

                colorObject.put("Value", base64);
                images.put(colorObject);
            }
            themeObject.put("Images", images);

            root.put("Theme", themeObject);

            // Constructs a FileWriter given a file name, using the platform's default charset
            File parent = new File(file.getParent());
            parent.mkdirs();
            FileWriter writer = new FileWriter(filename);
            writer.write(root.toString());
            writer.close();
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public void load(String filename) {
        reset();
        File file = new File(filename);
        if (file.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                JSONObject root = new JSONObject(builder.toString());
                JSONObject theme = root.getJSONObject("Theme");
                JSONArray colors = theme.getJSONArray("Colors");
                for (int i=0; i<colors.length(); i++) {
                    JSONObject pair = colors.getJSONObject(i);
                    String key = pair.getString("Key");
                    String value = pair.getString("Value");
                    if (pair.has("Alpha")) {
                        byte alpha = (byte)Integer.parseInt(pair.getString("Alpha"), 16);
                        setColor(key, Colors.getColor(value, alpha));
                    } else {
                        setColor(key, Colors.getColor(value));
                    }
                }
                if (theme.has("Images")) {
                    JSONArray images = theme.getJSONArray("Images");
                    for (int i = 0; i < images.length(); i++) {
                        JSONObject pair = images.getJSONObject(i);
                        String key = pair.getString("Key");
                        String base64 = pair.getString("Value");
                        BitmapDrawable drawAble = null;
                        if (pair.has("Type") && pair.getString("Type").toUpperCase().equals("FILE")) {
                            try {
                                String path = base64;
                                if (!path.startsWith("/")) {
                                    if (path.startsWith("./"))
                                        path = path.substring(2);
                                    path = file.getParent() + "/" + path;
                                }
                                Bitmap bmp = BitmapFactory.decodeFile(path);
                                if (bmp != null) {
                                    drawAble = new BitmapDrawable(bmp);
                                }
                            } catch (Exception exc) {

                            }
                        } else {
                            try {
                                byte[] data = Base64.decode(base64, Base64.DEFAULT);
                                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                                drawAble= new BitmapDrawable(bmp);
                            } catch (Exception exc) {

                            }
                        }
                        if (drawAble != null) {
                            setDrawable(key, drawAble);
                        }
                    }
                }
            }
            catch (Exception exc) {
                exc.printStackTrace();
                reset();
            }
            finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}
