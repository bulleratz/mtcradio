package greenmesh.settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SettingsRepository {
    public interface  SettingsRepositoryObserver {
        public void valueChanged(String name, Object value);
    }

    private ArrayList<SettingsHandler> _handlers = new ArrayList<>();
    private ArrayList<SettingsRepositoryObserver> _observers = new ArrayList<>();

    private SettingsHandler.SettingsObserver _observer = new SettingsHandler.SettingsObserver() {
        @Override
        public void valueChanged(SettingsHandler handler) {
            onValueChanged(handler);
        }
    };

    protected void onValueChanged(SettingsHandler handler) {
        notifyObservers(handler);
    }

    public void addObserver(SettingsRepositoryObserver observer) {
        if (!_observers.contains(observer))
            _observers.add(observer);
    }

    public void removeObserver(SettingsRepositoryObserver observer) {
        while (_observers.contains(observer)) {
            _observers.remove(observer);
        }
    }

    private void notifyObservers(SettingsHandler handler) {
        for (SettingsRepositoryObserver observer : _observers) {
            observer.valueChanged(handler.getName(), handler.getValue());
        }
    }

    public void addHandler(SettingsHandler handler) {
        if (handler != null && handler.getName() != null && !handler.getName().isEmpty()) {
            for (int i=0; i<_handlers.size(); i++) {
                if (_handlers.get(i).getName().equals(handler.getName())) {
                    _handlers.get(i).removeObserver(_observer);
                    _handlers.remove(i);
                    i--;
                }
            }
            _handlers.add(handler);
            handler.addObserver(_observer);
        }
    }


    public JSONObject toJson() throws JSONException {
        JSONObject root = new JSONObject();
        JSONArray array = new JSONArray();
        for (SettingsHandler handler : _handlers) {
            JSONObject handlerObject = new JSONObject();
            handlerObject.put("Name", handler.getName());
            handler.toJsonObject(handlerObject);
            array.put(handlerObject);
        }
        root.put("Settings", array);
        return root;
    }

    public void fromJson(JSONObject object) throws JSONException{
        JSONArray arr = object.getJSONArray("Settings");
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                if (obj != null && obj.has("Name")) {
                    String name = obj.getString("Name");
                    for (SettingsHandler handler : _handlers) {
                        if (handler.getName().equals(name)) {
                            handler.formJsonObject(obj);
                            break;
                        }
                    }
                }
            }
        }
    }

    public boolean setValue(String name, Object value) {
        for (SettingsHandler handler : _handlers) {
            if (handler.getName().equals(name)) {
                return handler.setValue(value);
            }
        }
        return false;
    }

    public Object getValue(String name, Object fallback) {
        for (SettingsHandler handler : _handlers) {
            if (handler.getName().equals(name)) {
                return handler.getValue();
            }
        }
        return fallback;
    }

}
