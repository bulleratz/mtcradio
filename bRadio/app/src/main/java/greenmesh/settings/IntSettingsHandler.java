package greenmesh.settings;

import android.content.Intent;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IntSettingsHandler extends SettingsHandler {

    public IntSettingsHandler(String name) {
        super(name);
    }
    public IntSettingsHandler(String name, int min, int max, int stepWidth) {
        super(name);
    }

    @Override
    public void toJsonObject(JSONObject object) throws JSONException {
        object.put(STR_VALUE, getValue());
    }

    @Override
    protected boolean acceptValue(Object value) {
        if (value.getClass().equals(Integer.class)) {
            return true;
        }
        return false;
    }

    @Override
    protected Object loadValue(JSONObject source) throws JSONException {
        setValue(source.getInt(STR_VALUE));
        return getValue();
    }

    public boolean setValue(int value) {
        return super.setValue(value);
    }

    public boolean setValue(byte value) {
        return super.setValue((int)value);
    }

    public boolean setValue(short value) {
        return super.setValue((int)value);
    }

    public int getIntValue() {
        Integer integer = (Integer)getValue();
        if (integer != 0) {
            return integer.intValue();
        }
        return 0;
    }
}
