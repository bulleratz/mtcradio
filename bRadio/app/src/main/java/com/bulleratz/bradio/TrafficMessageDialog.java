package com.bulleratz.bradio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class TrafficMessageDialog extends AppCompatActivity {

    RadioService _service = null;
    ImageView _ivStationIcon = null;
    TextView _tvSkip = null;
    TextView _tvClose = null;

    IRadioServiceListener _listener = new IRadioServiceListener() {
        @Override
        public void frequencyChanged(int frequency) {

        }

        @Override
        public void psnAvailable(String psn) {

        }

        @Override
        public void titleAvailable(String title) {

        }

        @Override
        public void ptyChanged(int pty) {

        }

        @Override
        public void piChanged(int pi) {

        }

        @Override
        public void stereoChanged(boolean stereo) {

        }

        @Override
        public void strengthChanged(int strength) {

        }

        @Override
        public void seekStart() {

        }

        @Override
        public void autoseekStart() {

        }

        @Override
        public void seekFound() {

        }

        @Override
        public void autoseekFound() {

        }

        @Override
        public void seekEnd() {

        }

        @Override
        public void autoseekEnd(RadioStationBouquet seekResult) {

        }

        @Override
        public void tpChanged(boolean tp) {

        }

        @Override
        public void taChanged(boolean ta) {
            if (!ta) {
                _delayHandler.sendEmptyMessageDelayed(0, 2000);
            }
        }

        @Override
        public void bandChanged() {

        }
    };

    private Handler _delayHandler = null;

    private ServiceConnection _connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _service = ((RadioServiceBinder) iBinder).getService();
            _ivStationIcon.setImageDrawable(RadioService.getBouquetManager().getSelectedStation().getImage());
            setTitle(RadioService.getBouquetManager().getSelectedStation().getDisplayName() + " Traffic Information");
            _service.addRadioServiceListener(_listener);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_traffic_message_dialog);

        _delayHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                finish();
            }
        };

        _ivStationIcon = (ImageView)findViewById(R.id.trafficDialogImgView);
        _tvSkip = (TextView)findViewById(R.id.trafficDialogSkip);
        _tvClose = (TextView)findViewById(R.id.trafficDialogClose);

        _tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TrafficMessageDialog.this.finish();
            }
        });

        _tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _service.skipTa();
                TrafficMessageDialog.this.finish();
            }
        });

        if (_service == null) {
            Intent intent = new Intent(this, RadioService.class);
            bindService(intent, _connection, BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onDestroy() {
        _service.removeRadioServiceListener(_listener);
        unbindService(_connection);
        super.onDestroy();
    }
}
