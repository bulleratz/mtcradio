package com.bulleratz.bradio;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import greenmesh.settings.BoolCheckboxSettingEditor;
import greenmesh.settings.IntSliderSettingsEditor;
import greenmesh.settings.SettingsEditor;

public class SettingsFragment extends Fragment {
    private class SettingsViewGroup extends LinearLayout {
        private LinearLayout _layout = null;
        private TextView _tvTitle = null;
        private String _title = "";

        public SettingsViewGroup(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            initView();
        }

        public SettingsViewGroup(Context context, AttributeSet attrs) {
            super(context, attrs);
            initView();
        }

        public SettingsViewGroup(Context context) {
            super(context);
            initView();
        }

        private void initView() {
            inflate(getContext(), R.layout.settings_group_layout, this);
            _layout = (LinearLayout)findViewById(R.id.rootLayout);
            _tvTitle = (TextView)findViewById(R.id.tvTitle);
            _tvTitle.setText(_title);
        }

        public void setTitle(String title) {
            _title = title;
            if (_title == null) {
                _title = "";
            }
            if (_tvTitle != null)
                _tvTitle.setText(_title);
        }

        public void addEditor(View view) {
           if (_layout != null && view != null) {
               LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)_layout.getLayoutParams();
               params.width = ViewGroup.LayoutParams.MATCH_PARENT;
               params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
               params.setMargins(16,2,2,2);
               _layout.addView(view, params);
           }
        }

    }
    private LinearLayout _layout = null;
    private HashMap<String, ArrayList<View>> _editors = new HashMap<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_vertical, container, false);
        _layout = view.findViewById(R.id.verticalLayout);

        ViewGroup.LayoutParams _params = _layout.getLayoutParams();

        Object[] keys = _editors.keySet().toArray();
        for (Object key : keys) {
            _params.width = LinearLayout.LayoutParams.MATCH_PARENT;
            _params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            SettingsViewGroup group = new SettingsViewGroup(getContext());
            group.setTitle(key.toString());
            _layout.addView(group, _params);
            ArrayList<View> editors = _editors.get(key);
            for (View editor : editors) {
                group.addEditor(editor);
            }
        }

        return view;
    }

    /* TODO: make protected and call from OnCreated... */
    public void addSettingsEditor(View editor, String group) {
        if (group == null || group.isEmpty()) {
            group = "";
        }
        if (!_editors.containsKey(group)) {
            ArrayList<View> editorList = new ArrayList<>();
            _editors.put(group, editorList);
        }
        _editors.get(group).add(editor);
    }
}
