package com.bulleratz.bradio;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class FrequencyBar extends View{
    private boolean RB_Flag = false;
    private int freq = 87500000;
    private Bitmap mBitmap = null;
    private OnFreqBarChangeListener mOnFreqBarChangeListener;
    private int max = 111500000;
    private int min = 83000000;
    private int rmax = 108000000;
    private int rmin = 87500000;
    private int step = 50000;

    /* renamed from: x */
    private int f0x = 0;

    public interface OnFreqBarChangeListener {
        void onMoveTrackingTouch(FrequencyBar freqBarView3);

        void onStartTrackingTouch(FrequencyBar freqBarView3);

        void onStopTrackingTouch(FrequencyBar freqBarView3);
    }

    public FrequencyBar(Context paramContext) {
        super(paramContext);
    }

    public FrequencyBar(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    public int getFreq() {
        return this.freq;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mBitmap != null) {
            this.mBitmap.recycle();
            this.mBitmap = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas paramCanvas) {
        if (this.mBitmap != null) {
            paramCanvas.drawBitmap(this.mBitmap, (float) (this.f0x - (this.mBitmap.getWidth() / 2)), 0.0f, new Paint());
        }
        super.onDraw(paramCanvas);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
        if (this.mBitmap == null) {
            this.mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.fb_ptr);
        }
        this.f0x = ((this.freq - this.min) * getWidth()) / (this.max - this.min);
    }

    /* access modifiers changed from: 0000 */
    public void onStartTrackingTouch() {
        if (this.mOnFreqBarChangeListener != null) {
            this.mOnFreqBarChangeListener.onStartTrackingTouch(this);
        }
    }

    /* access modifiers changed from: 0000 */
    public void onStopTrackingTouch() {
        if (this.mOnFreqBarChangeListener != null) {
            this.mOnFreqBarChangeListener.onStopTrackingTouch(this);
        }
    }

    /* access modifiers changed from: 0000 */
    public int getRealFreq() {
        if (this.RB_Flag) {
            if (this.freq < this.rmin) {
                this.freq = this.rmin;
            } else if (this.freq <= 74000000) {
                this.freq = (((this.freq - 65000000) / 30000) * 30000) + 65000000;
            } else if (this.freq < 81000000) {
                this.freq = 74000000;
            } else if (this.freq < 87500000) {
                this.freq = 87500000;
            } else if (this.freq <= 108000000) {
                this.freq = (((this.freq - 87500000) / 100000) * 100000) + 87500000;
            } else {
                this.freq = 108000000;
            }
        } else if (this.freq < this.rmin) {
            this.freq = this.rmin;
        } else if (this.freq > this.rmax) {
            this.freq = this.rmax;
        } else {
            this.freq = (((this.freq - this.rmin) / this.step) * this.step) + this.rmin;
        }
        return this.freq;
    }

    /* access modifiers changed from: 0000 */
    public void onMoveTrackingTouch() {
        if (this.mOnFreqBarChangeListener != null) {
            this.mOnFreqBarChangeListener.onMoveTrackingTouch(this);
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent) {
        switch (paramMotionEvent.getAction()) {
            case 0:
                onStartTrackingTouch();
                break;
            case 1:
            case 3:
                onStopTrackingTouch();
                break;
            case 2:
                this.freq = (((((int) paramMotionEvent.getX()) * ((this.max - this.min) / 1000)) / getWidth()) * 1000) + this.min;
                this.freq = getRealFreq();
                this.f0x = (((this.freq - this.min) / 1000) * getWidth()) / ((this.max - this.min) / 1000);
                invalidate();
                onMoveTrackingTouch();
                break;
        }
        return true;
    }

    public void setConf(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
        if (paramBoolean) {
            this.RB_Flag = false;
            this.rmin = paramInt1;
            this.rmax = paramInt2;
            this.step = paramInt3;
            this.min = 272800;
            this.max = 1960000;
            getBackground().setLevel(3);
            return;
        }
        this.rmin = paramInt1;
        this.rmax = paramInt2;
        this.step = paramInt3;
        if (this.rmin == 87500000 || this.rmin == 87600000) {
            this.RB_Flag = false;
            this.min = 83277000;
            this.max = 112100000;
            getBackground().setLevel(2);
        } else if (this.rmax == 74000000 || this.rmax == 90000000) {
            this.RB_Flag = false;
            this.min = 59850000;
            this.max = 95000000;
            getBackground().setLevel(0);
        } else {
            this.RB_Flag = true;
            this.min = 56142000;
            this.max = 116600000;
            getBackground().setLevel(1);
        }
    }

    public void setFreq(int paramInt) {
        if (freq != paramInt) {
            this.freq = paramInt;
            this.freq = getRealFreq();
            int b = (this.max - this.min) / 1000;
            this.f0x = (getWidth() * ((this.freq - this.min) / 1000)) / b;
            invalidate();
        }
    }

    public void setOnFreqBarChangeListener(OnFreqBarChangeListener paramOnFreqBarChangeListener) {
        this.mOnFreqBarChangeListener = paramOnFreqBarChangeListener;
    }
}
