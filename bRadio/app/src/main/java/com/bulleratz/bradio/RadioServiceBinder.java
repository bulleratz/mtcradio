package com.bulleratz.bradio;

import android.os.Binder;

/**
 * Created by maucher on 07.09.2017.
 */

public abstract class RadioServiceBinder extends Binder {
    public abstract RadioService getService();
}
