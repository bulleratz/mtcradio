package com.bulleratz.bradio;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.preference.PreferenceManager;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import greenmesh.settings.BoolSettingsHandler;
import greenmesh.settings.IntSettingsHandler;
import greenmesh.settings.SettingsRepository;

/**
 * Created by maucher on 06.10.2017.
 */

public class Settings {

    private static SettingsRepository _settingsRepo = new SettingsRepository();
    private static String _bouquetName = "";
    private static int _stationIndex = 0;
    private static int _frequency = 0;
    private static boolean _taOn = false;
    private static boolean _afOn = false;
    private static boolean _stereoOn = true;
    private static int _autoScanMinStrength = 30;

    static {
        _settingsRepo.addHandler(new IntSettingsHandler("AfThreshold", 0, 30, 10));
        _settingsRepo.setValue("AfThreshold", 30);
        _settingsRepo.addHandler(new BoolSettingsHandler("Fullscreen"));
    }



    public static void setBouquetName(String bouquetName) {
        _bouquetName = bouquetName;
        if (_bouquetName == null)
            _bouquetName = "";
    }

    public static String getBouquetName() {
        return _bouquetName;
    }

    public static void setStationIndex(int index) {
        _stationIndex = index;
    }

    public static int getStationIndex() {
        return _stationIndex;
    }

    public static int getFrerquency() {
        return _frequency;
    }

    public static void setFrequency(int frequency) {
        _frequency = frequency;
    }

    public static void setTaOn(boolean on) { _taOn = on; }

    public static boolean getTaOn() { return _taOn; }

    public static boolean getAfOn() { return _afOn; }

    public static void setAfOn(boolean on) { _afOn = on; }

    public static boolean getSteroOn() { return _stereoOn;}

    public static void setStereoOn(boolean on) { _stereoOn = on; }

    public static int getAutoScanThreshold() { return _autoScanMinStrength; }

    public static void setAutoScanThreshold(int threshold) { _autoScanMinStrength = threshold; }

    public static void storeSettings(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt("stationIndex", _stationIndex);
        edit.putString("bouquetName", _bouquetName);
        edit.putInt("frequency", _frequency);
        edit.putBoolean("taOn", _taOn);
        edit.putBoolean( "afOn", _afOn);
        edit.putBoolean("stereoOn", _stereoOn);
        edit.putInt("autoScanThreshold", _autoScanMinStrength);
        edit.commit();

        try {
            JSONObject root = _settingsRepo.toJson();
            File file = new File(context.getFilesDir() + "/settings.json");
            File parent = new File(file.getParent());
            parent.mkdirs();
            FileWriter writer = new FileWriter(context.getFilesDir() + "/settings.json");
            writer.write(root.toString());
            writer.close();
        } catch (Exception exc)
        {}
    }

    public static void restoreSettings(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        _stationIndex = preferences.getInt("stationIndex", 0);
        _bouquetName =  preferences.getString("bouquetName", "");
        _frequency = preferences.getInt("frequency", 87500000);
        _taOn = preferences.getBoolean("taOn", false);
        _afOn = preferences.getBoolean( "afOn", false);
        _stereoOn = preferences.getBoolean("stereoOn", true);
        _autoScanMinStrength = preferences.getInt("autoScanThreshold", 20);

        File file = new File(context.getFilesDir() + "/settings.json");
        BufferedReader reader = null;
        if (file.exists()) {
            try {
                reader = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONObject root = new JSONObject(builder.toString());
                _settingsRepo.fromJson(root);

            } catch (Exception exc) {
            } finally {
                try {
                    reader.close();
                } catch (Exception exc) {}
            }
        }
    }
}
