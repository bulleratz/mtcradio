package com.bulleratz.bradio;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import greenmesh.settings.SettingsRepository;

import static android.content.Context.BIND_AUTO_CREATE;

public class InfoBar extends LinearLayout {

    public static final int TA_VISIBLE = 0x01;
    public static final int TP_VISIBLE = 0x02;
    public static final int ST_VISIBLE = 0x04;
    public static final int CLOCK_VISIABLE = 0x08;
    public static final int PI_VISIBLE = 0x10;
    public static final int FREQ_VISIBLE = 0x20;
    public static final int PTY_VISIBLE = 0x40;
    public static final int BOUQUET_SELECTION_VISIBLE = 0x80;
    public static final int ALL_VISIBLE = 0x7fffffff;
    public static final int VISIBLE_FLAG = 0x80000000;

    private static List<Integer> _itemList = null;
    public static synchronized List<Integer> getItemList() {
        if (_itemList == null ) {
            _itemList = new ArrayList<Integer>();
            _itemList.add(TA_VISIBLE);
            _itemList.add(TP_VISIBLE);
            _itemList.add(ST_VISIBLE);
            _itemList.add(CLOCK_VISIABLE);
            _itemList.add(PI_VISIBLE);
            _itemList.add(FREQ_VISIBLE);
            _itemList.add(PTY_VISIBLE);
            _itemList.add(BOUQUET_SELECTION_VISIBLE);
        }
        return _itemList;
    }

    private AutoSizeTextView _clockView = null;
    private AutoSizeTextView _ptyView = null;

    private ServiceConnection _connection = null;
    private RadioService _service = null;

    private ImageView ivTa = null;
    private ImageView ivStereo = null;
    private ImageView ivTp = null;
    private AutoSizeTextView tvPi = null;
    private AutoSizeTextView tvFrequency = null;
    private LinearLayout parentLayout = null;
    private View _bouquetSelection = null;
    private int position = 0; // Center
    private int barType = 0; // Upper Left

    private static final int POSITION_CENTER = 0;
    private static final int POSITION_LEFT = 1;
    private static final int POSITION_RIGHT = 2;

    private Handler clockHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            SimpleDateFormat df = null;
            if (android.text.format.DateFormat.is24HourFormat(getContext())) {
                df = new SimpleDateFormat("HH:mm");
            } else {
                df = new SimpleDateFormat("hh:mm");
            }
            String currentDateTimeString = df.format(new Date());


            // textView is the TextView view that should display it
            if (_clockView != null)
                _clockView.setText(currentDateTimeString);

            this.sendEmptyMessageDelayed(0, 1000);
        }
    };

    private BouquetManager.IBouquetManagerListener _bouquetListener = new BouquetManager.IBouquetManagerListener() {
        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {
            if (_bouquetSelection != null) {
                //_bouquetSelection.setText(RadioService.getBouquetManager().getSelectedBouquet().getName());
            }
        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {

        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {

        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {

        }
    };



    SettingsRepository.SettingsRepositoryObserver _settingsObserver = new SettingsRepository.SettingsRepositoryObserver() {
        @Override
        public void valueChanged(String name, Object value) {
            if (name.equals(_settingsId)) {
                //Integer i = (Integer) value;
                //if (i != null) {
                    setVisibility();
                //}
            }
        }
    };

    private IRadioServiceListener _listener = new IRadioServiceListener() {
        @Override
        public void frequencyChanged(int frequency) {
            if (tvFrequency != null) {
                if (_service != null) {
                    if (_service.isFm()) {
                        float freeq = (float)_service.getFrequency() / 1000000.0f;
                        tvFrequency .setText((Float.toString(freeq)) + "Mhz");
                    } else {
                        float freeq = _service.getFrequency() / 1000f;
                        tvFrequency .setText((Float.toString(freeq)) + "Khz");
                    }
                }
            }
        }

        @Override
        public void psnAvailable(String psn) {

        }

        @Override
        public void titleAvailable(String title) {

        }

        @Override
        public void ptyChanged(int pty) {
            String[] ptyShort = getResources().getStringArray(R.array.pty_names);
            if (pty >= 0 && pty < ptyShort.length) {
                if (_ptyView != null) {
                    _ptyView.setText(ptyShort[pty]);
                }
            }
        }

        @Override
        public void piChanged(int pi) {
            if (tvPi != null) {
                tvPi.setText("PI: 0x" + Integer.toString(_service.getPi(), 16));
            }
        }

        @Override
        public void stereoChanged(boolean stereo) {
            if (ivStereo != null) {
                if (stereo)
                    ivStereo.setColorFilter(getResources().getColor(R.color.enabled_tint));
                else
                    ivStereo.setColorFilter(getResources().getColor(R.color.disabled_tint));
            }
        }

        @Override
        public void strengthChanged(int strength) {

        }

        @Override
        public void seekStart() {

        }

        @Override
        public void autoseekStart() {

        }

        @Override
        public void seekFound() {

        }

        @Override
        public void autoseekFound() {

        }

        @Override
        public void seekEnd() {

        }

        @Override
        public void autoseekEnd(RadioStationBouquet seekResult) {

        }

        @Override
        public void tpChanged(boolean tp) {
            if (ivTp != null) {
                if (tp)
                    ivTp.setColorFilter(getResources().getColor(R.color.enabled_tint));
                else
                    ivTp.setColorFilter(getResources().getColor(R.color.disabled_tint));
            }
        }

        @Override
        public void taChanged(boolean ta) {
            if (ivTa != null) {
                if (ta)
                    ivTa.setColorFilter(getResources().getColor(R.color.enabled_tint));
                else
                    ivTa.setColorFilter(getResources().getColor(R.color.disabled_tint));
            }
        }

        @Override
        public void bandChanged() {

        }
    };

    public InfoBar(Context context) {
        super(context);
        init();
    }

    public InfoBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        loadAttributes(context, attrs);
        init();
    }

    public InfoBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadAttributes(context, attrs);
        init();
    }

    public InfoBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        loadAttributes(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.info_bar, this);
        RadioService.getBouquetManager().addBouquetManagerListener(_bouquetListener);

        /*ivTa = (ImageView)findViewById(R.id.ivTrafficAnnouncement);
        ivStereo = (ImageView)findViewById(R.id.ivStereo);
        ivTa = (ImageView)findViewById(R.id.ivTrafficProgram);
        tvPi = (AutoSizeTextView)findViewById(R.id.tvPi);
        _clockView = findViewById(R.id.tvClock);*/
        parentLayout = findViewById(R.id.layout_infoBar);


        UiSettings.init(getContext());
        setVisibility();
        UiSettings.addObserver(_settingsObserver);

        addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View view) {
                bindService();
            }

            @Override
            public void onViewDetachedFromWindow(View view) {
                unbindService();
            }
        });

    }

    public void bindService() {
        if (_connection == null) {
            _connection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    _service = ((RadioServiceBinder)iBinder).getService();
                    _service.addRadioServiceListener(_listener);

                    updateAll();
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    if (_service != null) {
                        _service.removeRadioServiceListener(_listener);
                        _service = null;
                    }
                }
            };
            Intent intent = new Intent(this.getContext(), RadioService.class);
            getContext().bindService(intent, _connection, BIND_AUTO_CREATE);
        }

        if (_service != null)
            updateAll();
    }

    public void unbindService() {
        if (_connection != null) {
            ServiceConnection temp = _connection;
            _service = null;
            _connection = null;
            try {
                getContext().unbindService(temp);
            } catch (Exception exc) {
            }

        }
    }

    private void updateAll() {
        if (_service != null) {
            _listener.stereoChanged(_service.isStereo());
            _listener.taChanged(_service.getTa());
            _listener.tpChanged(_service.getTp());
            _listener.frequencyChanged(_service.getFrequency());
            _listener.piChanged(_service.getPi());
            _listener.ptyChanged(_service.getPty());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        unbindService();
        RadioService.getBouquetManager().removeBouquetManagerListener(_bouquetListener);
        super.finalize();
    }

    private String _settingsId = "";

    void loadAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.InfoBar,
                0, 0);

        try {
            position = a.getInteger(R.styleable.InfoBar_position, 0);
            barType = a.getInteger(R.styleable.InfoBar_barType, 0);
        } finally {
            a.recycle();
        }
        switch (barType) {
            case 0:
                _settingsId = UiSettings.TOP_LEFT_INFO_BAR_ITEMS;
                break;
            case 1:
                _settingsId = UiSettings.TOP_CENTER_INFO_BAR_ITEMS;
                break;
            case 2:
                _settingsId = UiSettings.TOP_RIGHT_INFO_BAR_ITEMS;
                break;
            case 3:
                _settingsId = UiSettings.BOTTOM_CENTER_INFO_BAR_ITEMS;
                break;
            default:
                _settingsId = "";

        }
        //setVisibility();
    }

    private ImageView addImageView(int drawableId) {
        ImageView view = new ImageView(getContext());
        LayoutParams parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        parms.setMargins(2, 2, 2, 2);
        parms.gravity = Gravity.CENTER_VERTICAL;
        view.setImageDrawable(getResources().getDrawable(drawableId));
        view.setAdjustViewBounds(true);
        parentLayout.addView(view, parms);
        return view;
    }

    private View addClockView() {
        clockHandler.removeMessages(0);
        _clockView = addTextView();
        clockHandler.sendEmptyMessage(0);
        return _clockView;
    }

    private AutoSizeTextView addTextView() {
        AutoSizeTextView view = new AutoSizeTextView(getContext());
        view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
        LayoutParams parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        parms.setMargins(2, 2, 2, 2);
        view.setGravity(Gravity.CENTER);
        parentLayout.addView(view, parms);
        return view;
    }

    private class BouquetSelectionView extends LinearLayout {
        AutoSizeTextView textView = null;
        ImageView imgView = null;
        RadioStationBouquet _selectedBouquet = null;
        public BouquetSelectionView(Context context) {
            super(context);
            _selectedBouquet = RadioService.getBouquetManager().getSelectedBouquet();

            textView = new AutoSizeTextView(getContext());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
            LayoutParams parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
            parms.setMargins(2, 2, 2, 2);
            textView.setGravity(Gravity.CENTER);
            addView(textView, parms);
            textView.setClickable(true);
            textView.setOnClickListener(_bouquetSelectionListener);
            if (_selectedBouquet != null) {
                textView.setText(_selectedBouquet.getName());
                _selectedBouquet.addObserver(_bouquetObserver);
            }

            imgView = new ImageView(getContext());
            parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            parms.setMargins(2, 2, 0, 2);
            parms.gravity = Gravity.CENTER_VERTICAL;
            imgView.setImageDrawable(getResources().getDrawable(R.drawable.baseline_arrow_drop_down_24));
            imgView.setAdjustViewBounds(true);
            imgView.setClickable(true);
            imgView.setOnClickListener(_bouquetSelectionListener);
            addView(imgView, parms);

            RadioService.getBouquetManager().addBouquetManagerListener(_bouquetManagerListener);
        }

        private OnClickListener _bouquetSelectionListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getResources().getString(R.string.select_bouquet));
                int count = RadioService.getBouquetManager().getBouquetCount();
                String name[] = new String[count];
                for (int i=0; i<count; i++) {
                    name[i] = RadioService.getBouquetManager().getBouquet(i).getName();
                }
                builder.setItems(name, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RadioService.getBouquetManager().setSelectedBouqet(RadioService.getBouquetManager().getBouquet(i));
                    }
                });
                builder.show();
            }
        };

        RadioStationBouquet.RadioStationBouqetObserver _bouquetObserver = new RadioStationBouquet.RadioStationBouqetObserver() {
            @Override
            public void stationAdded(int index, RadioStation station) {

            }

            @Override
            public void stationRemoved(int index, RadioStation station) {

            }

            @Override
            public void nameChanged(String name) {
                if (textView != null) {
                    textView.setText(name);
                }
            }
        };

        private BouquetManager.IBouquetManagerListener _bouquetManagerListener = new BouquetManager.IBouquetManagerListener() {
            @Override
            public void selectedBouquetChanged(RadioStationBouquet bouquet) {
                if (_selectedBouquet != null)
                    _selectedBouquet.removeObserver(_bouquetObserver);
                _selectedBouquet = bouquet;
                if (textView != null && bouquet != null) {
                    textView.setText(RadioService.getBouquetManager().getSelectedBouquet().getName());
                    _selectedBouquet.addObserver(_bouquetObserver);
                }
            }

            @Override
            public void selectedStationChanged(RadioStation station, int stationIndex) {

            }

            @Override
            public void bouquetAdded(RadioStationBouquet bouquet) {

            }

            @Override
            public void bouquetRemoved(RadioStationBouquet bouquet) {

            }
        };



        @Override
        protected void finalize() throws Throwable {
            if (imgView != null)
                imgView.setOnClickListener(null);
            if (textView != null)
                textView.setOnClickListener(null);
            RadioService.getBouquetManager().removeBouquetManagerListener(_bouquetManagerListener);
            super.finalize();
        }
    }

    private View addBouquetSelection() {
        /*LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        parentLayout.addView(linearLayout, parms);

        AutoSizeTextView view = new AutoSizeTextView(getContext());
        view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
        parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        parms.setMargins(2, 2, 2, 2);
        view.setGravity(Gravity.CENTER);
        linearLayout.addView(view, parms);
        view.setClickable(true);
        view.setOnClickListener(_bouquetSelectionListener);
        view.setText(RadioService.getBouquetManager().getSelectedBouquet().getName());

        ImageView imgView = new ImageView(getContext());
        parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        parms.setMargins(2, 2, 0, 2);
        parms.gravity = Gravity.CENTER_VERTICAL;
        imgView.setImageDrawable(getResources().getDrawable(R.drawable.baseline_arrow_drop_down_24));
        imgView.setAdjustViewBounds(true);
        linearLayout.addView(imgView, parms);

        return linearLayout;*/
        BouquetSelectionView view = new BouquetSelectionView(getContext());
        LayoutParams parms = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        parentLayout.addView(view, parms);
        return view;
    }

    public synchronized void setVisibility() {

            parentLayout.removeAllViews();
            switch (position) {
                case POSITION_LEFT:
                    parentLayout.setGravity(Gravity.LEFT);
                    break;
                case POSITION_RIGHT:
                    parentLayout.setGravity(Gravity.RIGHT);
                    break;
                default:
                    parentLayout.setGravity(Gravity.CENTER);
                    break;
            }


            ivTa = null;
            ivStereo = null;
            ivTp = null;
            _clockView = null;
            tvPi = null;
            tvFrequency = null;
            if (_bouquetSelection != null) {
                _bouquetSelection.setOnClickListener(null);
                _bouquetSelection = null;
            }

        List<Integer> items = null;
           switch (barType) {
               case 0:
                   items = UiSettings.getTopLeftInfoBarItems();
                   break;
               case 1:
                   items = UiSettings.getTopCenterBarItems();
                   break;
               case 2:
                   items = UiSettings.getTopRightBarItems();
                   break;
               case 3:
                   items = UiSettings.getBottomCenterBarItems();
                   break;
               default:
                   items = new ArrayList<>();
           }


    for (int i=0; i< items.size(); i++) {
        if ((items.get(i) == (TA_VISIBLE | 0x80000000))) {
            ivTa = addImageView(R.drawable.baseline_info_white_48);
        }
        if ((items.get(i) == (ST_VISIBLE | 0x80000000))) {
            ivStereo = addImageView(R.drawable.stereo);
        }
        if ((items.get(i) == (TP_VISIBLE  | 0x80000000))) {
            ivStereo = addImageView(R.drawable.baseline_drive_eta_white_48);
        }
        if ((items.get(i) == (CLOCK_VISIABLE  | 0x80000000))) {
            addClockView();
        }
        if ((items.get(i) == (FREQ_VISIBLE  | 0x80000000))) {
            tvFrequency = addTextView();
        }

        if ((items.get(i) == (PI_VISIBLE | 0x80000000))) {
            tvPi = addTextView();
        }

        if ((items.get(i) == (PTY_VISIBLE | 0x80000000))) {
            _ptyView = addTextView();
        }
        if ((items.get(i) == (BOUQUET_SELECTION_VISIBLE | 0x80000000))) {
            _bouquetSelection = addBouquetSelection();
        }

    }
        updateAll();
    }
}
