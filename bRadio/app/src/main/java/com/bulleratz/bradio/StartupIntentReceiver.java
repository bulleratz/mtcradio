package com.bulleratz.bradio;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.security.Permission;

/**
 * Created by maucher on 13.09.2017.
 */

public class StartupIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("BRadio", "StartupIntentReceiver called");
        Intent myIntent = new Intent(context, RadioService.class);
        ComponentName name = context.startService( myIntent);
        if (name != null)
            Log.d("BRadio", "StartService returned " + name.getClassName());
        else
            Log.d("BRadio", "StartService returned nothing!");
        //completeWakefulIntent(intent);
    }
}
