package com.bulleratz.bradio;

public class MtcFactorySettings {

    private static final int INDEX_RADIO_GAIN = 99; /* Auto search stop - DX - AF threshold */
    private static final int INDEX_RADIO_APP = 60; /* Radio APP yes no */

    private byte[] data = null;

    public void load(RadioAbstractionLayer radio) {
        String parameters = radio.getParameters("cfg_factory=");
        data = null;
        if (parameters != null && !parameters.isEmpty()) {
            String[] split = parameters.split(",");
            int length = split.length;
            data = new byte[length];
            for (int i = 0; i < length; i++) {
                try {
                    data[i] = (byte) (Integer.parseInt(split[i]) & 0xff);
                } catch (NumberFormatException exc) {

                }
            }
        }
    }

    public int getRadioGain() {
        if (data != null && data.length > INDEX_RADIO_GAIN)
            return data[INDEX_RADIO_GAIN];
        return -1;
    }

    public void setRadioGain(byte gain) {
        if (data != null && data.length > INDEX_RADIO_GAIN) {
            data[INDEX_RADIO_GAIN] = gain;
        }
    }

    public int getRadioApp() {
        if (data != null && data.length > INDEX_RADIO_APP)
            return data[INDEX_RADIO_APP];
        return -1;
    }

    public void setRadioApp(byte app) {
        if (data != null && data.length > INDEX_RADIO_APP) {
            data[INDEX_RADIO_APP] = app;
        }
    }

    public String toString(String separator) {
        StringBuffer buffer = new StringBuffer();
        if (data != null) {
            for (int i = 0; i < data.length; i++) {
                buffer.append(data[i]);
                if (i != data.length - 1) {
                    buffer.append(separator);
                }
            }
        } else {
            buffer.append(">> Factory Settings Not Loaded <<");
        }
        return buffer.toString();
    }

    public String toString() {
        return toString(",");
    }

    public void save(RadioAbstractionLayer radio) {
        String data = toString();
        radio.setParameters("cfg_factory=" + data);
    }
}
