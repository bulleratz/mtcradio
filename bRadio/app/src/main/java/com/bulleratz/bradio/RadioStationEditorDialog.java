package com.bulleratz.bradio;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class RadioStationEditorDialog extends Dialog {
    private RadioStation _staion = null;
    private int _stationIndex = -1;

    private  EditText etDisplayName = null;
    private EditText etRdsName = null;
    private EditText etFrequency = null;
    private Spinner spImage = null;
    private StationImageAdapter _spinnerAdapter = null;
    private RadioService _service = null;
    private Button btnOk = null;
    private Button btnCancel = null;
    private Button btnCopyRds = null;
    private Button btnFindImage = null;

    private ServiceConnection _connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _service = ((RadioServiceBinder) iBinder).getService();
            if (_service != null) {
                etFrequency.setText(Double.toString(_service.getFrequency() / 1000000.0) + " Mhz");
                etRdsName.setText(_service.getPsn());
                if (_service.getFrequency() == _staion.getFrequency()) {
                    etDisplayName.setText(_staion.getUserDefinedName());
                    setSpinnerImage();
                }
                int index = guessStationImage(etDisplayName.getText().toString());
                if (index == 0)
                    index = guessStationImage(etRdsName.getText().toString());
                if (index != 0)
                    spImage.setSelection(index);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    private String normalize(String stationName) {
        stationName = stationName.toUpperCase();
        for (int ch=0; ch<stationName.length(); ch++) {
            if ((stationName.charAt(ch) < 48 || stationName.charAt(ch) > 57) && ((stationName.charAt(ch) < 65 || stationName.charAt(ch) > 90)) ) {
                stationName = stationName.replace("" + stationName.charAt(ch), "");
                ch = 0;
            }
        }
        return stationName;
    }

    private int guessStationImage(String stationName) {
        int position = 0;
        if (stationName == null)
            return position;
        stationName = normalize(stationName);

        int matchPrio = 0;

        for (int i = 0; i < _spinnerAdapter.getCount(); i++) {
            String path = _spinnerAdapter.getItem(i);
            int index = path.lastIndexOf("/");
            if (index > -1) {
                path = path.substring(index + 1);
            }
            index = path.lastIndexOf(".");
            if (index > -1) {
                path = path.substring(0, index);
            }
            path = normalize(path);

            /*Exact match */
            if (path.toUpperCase().equals(stationName) && matchPrio < 3) {
                spImage.setSelection(i);
                matchPrio = 3;
                position = i;
            }

            /* No exact match - contains*/
            if (path.contains(stationName) && matchPrio < 1) {
                spImage.setSelection(i);
                matchPrio = 1;
                position = i;
            }

            /* No exact match - starts with*/
            if (path.startsWith(stationName) && matchPrio < 2) {
                spImage.setSelection(i);
                matchPrio = 2;
                position = i;
            }


        }
        return position;
    }

    public RadioStationEditorDialog(Activity a, RadioStation station, int stationIndex) {
        super(a);
        _staion = station;
        _stationIndex = stationIndex;



    }

    public void show() {
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        super.show();
        getWindow().setAttributes(lp);
    }

    private void setSpinnerImage() {
        for (int i=0; i<_spinnerAdapter.getCount(); i++) {
            if (_spinnerAdapter.getItem(i).equals(_staion.getUserDefinedImage())) {
                spImage.setSelection(i);
                return;
            }
        };
        spImage.setSelection(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_radio_station_editor);

        etDisplayName = (EditText)findViewById(R.id.etStationEditDisplayName);
        etRdsName = (EditText)findViewById(R.id.etStationEditRdsName);
        etFrequency = (EditText)findViewById(R.id.etStationEditFrequency);
        spImage = (Spinner) findViewById(R.id.etStationEditDisplayIcon);
        btnOk = (Button)findViewById(R.id.btnOk);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnCopyRds = (Button)findViewById(R.id.btnStationEditorCopyRds);
        btnFindImage = (Button)findViewById(R.id.btnStationEditorFind);
        _spinnerAdapter = new StationImageAdapter(getContext(), Constants.LOGO_STORAGE_PATH);
        spImage.setAdapter(_spinnerAdapter);
        spImage.setFocusable(true);
        spImage.setFocusableInTouchMode(true);
        spImage.setClickable(true);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_service != null) {
                    RadioService.getBouquetManager().updateStation(_stationIndex, _service.getFrequency(), etDisplayName.getText().toString(), _spinnerAdapter.getItem(spImage.getSelectedItemPosition()), _service.getPi());
                }
                dismiss();
            }
        });

        btnCopyRds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDisplayName.setText(etRdsName.getText());
            }
        });

        btnFindImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = guessStationImage(etDisplayName.getText().toString());
                if (index != 0) {
                    spImage.setSelection(index);
                }
            }
        });

        Context context = getContext();

        if (_service == null && context != null) {
            Intent intent = new Intent(context, RadioService.class);
            context.bindService(intent, _connection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        if (_connection != null) {
            getContext().unbindService(_connection);
            _connection = null;
        }
    }
}
