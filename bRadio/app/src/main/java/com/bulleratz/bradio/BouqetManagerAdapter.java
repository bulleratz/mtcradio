package com.bulleratz.bradio;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BouqetManagerAdapter extends ArrayAdapter<String> implements BouquetManager.IBouquetManagerListener {
    private BouquetManager _manager = null;

    public BouqetManagerAdapter(@NonNull Context context, BouquetManager mgr) {
        super(context, -1);
        _manager = mgr;
        _manager.addBouquetManagerListener(this);
    }

        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {

        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {

        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {
            notifyDataSetChanged();
        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {
            notifyDataSetChanged();
        }

    @Override
    protected void finalize() throws Throwable {
        _manager.removeBouquetManagerListener(this);
        super.finalize();
    }

    @Override
    public int getCount() {
        return _manager.getBouquetCount();
    }

    private class ViewHolder implements RadioStationBouquet.RadioStationBouqetObserver {
        RadioStationBouquet bouquet = null;
        TextView tv = null;

        @Override
        public void stationAdded(int index, RadioStation station) {

        }

        @Override
        public void stationRemoved(int index, RadioStation station) {

        }

        @Override
        public void nameChanged(String name) {
            if (tv != null)
                tv.setText(name);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.string_item, parent, false);
            holder = new ViewHolder();
            holder.tv = (TextView)convertView.findViewById(R.id.string_item_text_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        RadioStationBouquet bouquet = _manager.getBouquet(position);
        if (holder.bouquet != bouquet) {
            if (holder.bouquet != null)
                holder.bouquet.removeObserver(holder);
            holder.bouquet = bouquet;
            if (holder.bouquet != null)
                holder.bouquet.addObserver(holder);
        }

        String text = bouquet.getName();

        if (text == null || text.isEmpty()) {
            text = "N/A";
        }
        if (holder.tv != null)
            holder.tv.setText(text);
        return convertView;
    }

    public RadioStationBouquet getBouquet(int index) {
        if (index >= 0 && index <_manager.getBouquetCount()) {
            return _manager.getBouquet(index);
        }
        return null;
    }

    public String getItem(int index) {
        if (index >= 0 && index <_manager.getBouquetCount()) {
            return _manager.getBouquet(index).getName();
        }
        return "";
    }
}
