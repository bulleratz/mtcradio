package com.bulleratz.bradio;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Process;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

public class CrashHandler implements UncaughtExceptionHandler {
    private static CrashHandler mInstance;
    private final String CARSH_DIR_PATH = "/mnt/sdcard/carsh/";
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
    private HashMap<String, String> infos;
    private Context mContext;
    private UncaughtExceptionHandler mDefaultHandler;
    private String pkgName;

    public CrashHandler(Context context) {
        this.mContext = context.getApplicationContext();
        this.pkgName = this.mContext.getPackageName().replace(".", "_");
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public static CrashHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CrashHandler(context);
        }
        return mInstance;
    }

    private boolean handleException(Throwable throwable) {
        if (throwable == null) {
            return false;
        }
        collectInfo();
        saveCrashInfoToFile(throwable);
        return true;
    }

    void collectInfo() {
        if (this.infos == null) {
            this.infos = new HashMap();
        }
        try {
            PackageInfo packageinfo = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 1);
            if (packageinfo != null) {
                this.infos.put("versionName", packageinfo.versionName);
                this.infos.put("versionCode", "" + packageinfo.versionCode);
            }
        } catch (NameNotFoundException e) {
        }
    }

    private String saveCrashInfoToFile(Throwable ex) {
        Writer info = new StringWriter();
        PrintWriter printWriter = new PrintWriter(info);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String result = info.toString();
        printWriter.close();
        StringBuffer sb = new StringBuffer();
        for (Entry entry : this.infos.entrySet()) {
            sb.append(entry.getKey() + "=" + entry.getValue() + "\n");
        }
        sb.append(result);
        File crashdir = new File("/mnt/sdcard/carsh/");
        if (!crashdir.exists()) {
            crashdir.mkdirs();
        }
        String filename = this.pkgName + "-" + this.dateFormat.format(new Date()) + ".log";
        try {
            FileOutputStream outstream = new FileOutputStream("/mnt/sdcard/carsh/" + filename);
            outstream.write(sb.toString().getBytes());
            outstream.close();
            outstream.close();
            return filename;
        } catch (Exception e) {
            return null;
        }
    }

    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
        if (handleException(paramThrowable)) {
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }
        if (this.mDefaultHandler == null) {
            this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        }
        this.mDefaultHandler.uncaughtException(paramThread, paramThrowable);
    }
}
