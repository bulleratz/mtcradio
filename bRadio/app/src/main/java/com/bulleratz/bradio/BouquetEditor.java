package com.bulleratz.bradio;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentTransaction;
import android.app.Service;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;

public class BouquetEditor extends FragmentActivity {



    FrameLayout _leftFrame = null;
    FrameLayout _rightFrame = null;

    BouqetEditorPanel _leftEditor = null;
    BouqetEditorPanel _rightEditor = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bouquet_editor);

        _leftFrame = (FrameLayout)findViewById(R.id.frameLayoutLeft);
        _rightFrame = (FrameLayout)findViewById(R.id.frameLayoutRight);

        _leftEditor = new BouqetEditorPanel();
        _rightEditor = new BouqetEditorPanel();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayoutLeft, _leftEditor);
        transaction.commit();

        transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayoutRight, _rightEditor);
        transaction.commit();

        ImageButton btnRtl = (ImageButton)findViewById(R.id.bouquet_editor_btnrtl);
        ImageButton btnLtr = (ImageButton)findViewById(R.id.bouquet_editor_btnltr);

        btnRtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                copyStation(_rightEditor, _leftEditor);
            }
        });

        btnLtr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                copyStation(_leftEditor, _rightEditor);
            }
        });
    }

    private void copyStation(BouqetEditorPanel source, BouqetEditorPanel target) {
        RadioStation station = source.getSelectedStation();
        RadioStationBouquet bouqet = target.getBouqet();
        if (station != null && bouqet != null) {
            bouqet.add(station);
        }
        BouquetManager manager = RadioService.getBouquetManager();
        manager.saveBouquets();
    }
}
