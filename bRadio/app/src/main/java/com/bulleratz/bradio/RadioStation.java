package com.bulleratz.bradio;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.width;

/**
 * Created by maucher on 06.09.2017.
 */

public class RadioStation implements Serializable{

    int _radioFrequency = 87500000;
    ArrayList<Integer> _alternativeFrequencies = new ArrayList<Integer>();
    Boolean _alternativeFrequencieEnabled = false;
    private String _name = "";
    private String _userDefinedName = "";
    private String _userDefinedImage = "";
    private transient Drawable _drawable = null;
    private transient Bitmap _bitmap = null;
    private transient  ArrayList<IRadioStationListener> _listeners = new ArrayList<IRadioStationListener>();
    private transient  String _currentTitle = "";
    private transient  String _imagePath = "";
    private transient  int _pi = 0;
    private int _currentFrequency = 0;

    public RadioStation clone() {
        RadioStation station = new RadioStation();
        station._radioFrequency = _radioFrequency;
        station._alternativeFrequencies.addAll(_alternativeFrequencies);
        station._alternativeFrequencieEnabled = _alternativeFrequencieEnabled;
        station._name = _name;
        station._userDefinedName = _userDefinedName;
        station._userDefinedImage = _userDefinedImage;
        station._pi = _pi;

        return station;
    }

    private final String xmlElementName = "RadioStation";

    public String toXml() {
        String str = "<RadioStation name=\"" + _name + "\" userName=\"" + _userDefinedName + "\" image=\"" + _userDefinedImage + "\" frquency=\"" + _radioFrequency + "\">";
        for (int i=0; i<_alternativeFrequencies.size(); i++) {
            str += "<Alternative frequency=\"" + _alternativeFrequencies.get(i) + "\">";
        }
        str += "</RadioStation>";
        return str;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject station = new JSONObject();

        station.put("Name", _name);
        station.put("UserName", _userDefinedName);
        station.put("Frequency", _radioFrequency);
        station.put( "Icon", _userDefinedImage);
        station.put("Pi", _pi);
        station.put("CurrentFrequency", _currentFrequency);
        JSONArray altFreqs = new JSONArray();
        for (int i=0; i<_alternativeFrequencies.size(); i++) {
            JSONObject altFreq = new JSONObject();
            altFreq.put("Frequency", _alternativeFrequencies.get(i).intValue());
            altFreqs.put(altFreq);
        }
        station.put("Alternatives", altFreqs);

        return station;
    }

    public void setCurrentFrequency(int frequency) {
        _currentFrequency = frequency;
        if (_currentFrequency != 0)
            addFrequency(frequency);
        notifyChanged();
    }

    public void resetCurrentFrequency() {
        setCurrentFrequency(0);
    }

    public int getCurrentFrequency() {
        return _currentFrequency;
    }

    public static RadioStation fromJson(JSONObject object) {
        RadioStation station = new RadioStation();
        try {
            station._name = object.getString("Name");
            station._radioFrequency = object.getInt("Frequency");
            station._userDefinedImage = object.getString("Icon");
            station._userDefinedName = object.getString("UserName");
            JSONArray alts = object.getJSONArray("Alternatives");
            for (int i=0; i<alts.length(); i++) {
                JSONObject freq = alts.getJSONObject(i);
                station._alternativeFrequencies.add(freq.getInt("Frequency"));
            }
            if (object.has("Pi")) {
                station._pi = object.getInt("Pi");
            }
            if (object.has("CurrentFrequency")) {
                station._currentFrequency = object.getInt("CurrentFrequency");
            }
        } catch (Exception exc) {
            return null;
        }
        return station;
    }


    private ArrayList<IRadioStationListener> getListeners() {
        if (_listeners == null)
            _listeners = new ArrayList<IRadioStationListener>();
        return _listeners;
    }

    public RadioStation() {

    }

    public void setCurrentTitle(String title)
    {
        if (!getCurrentTitle().equals(title)) {
            _currentTitle = title;
            notifyChanged();
        }
    }

    public String getCurrentTitle() {
        if (_currentTitle == null)
            _currentTitle = "";
        return _currentTitle;
    }

    public void addRadioStationListener(IRadioStationListener listener) {
        if (!getListeners().contains(listener))
            getListeners().add(listener);
    }

    public void removeRadioStationListener(IRadioStationListener listener) {
        while (getListeners().contains(listener))
            getListeners().remove(listener);
    }

    private void notifyChanged() {
        for (IRadioStationListener listener : getListeners())
            listener.radioStationChanged(this);
    }

    public String getDisplayName()
    {
        if (getUserDefinedName() != null && !getUserDefinedName().isEmpty())
            return getUserDefinedName();
        if (getName() == null || getName().isEmpty()) {
            if (_radioFrequency >= 87000000)
                return Double.toString(((double) _radioFrequency) / 1000000); //FM - MHz
            else
                return Double.toString(((double) _radioFrequency) / 1000); //AM kHz
        }
        return getName();
    }

    public String getName()
    {
        if (_name == null)
            _name = "";
        return _name;
    }

    public String getUserDefinedName() {
        if (_userDefinedName == null)
            _userDefinedName = "";
        return _userDefinedName;
    }

    public void setName(String name)
    {
        if (name == null)
            name = "";
        name.trim();
        if (!name.equals(_name))
        {
            _drawable = null;
            _name = name;
            notifyChanged();
        }
    }

    public int getFrequency()
    {
        if (_currentFrequency != 0) {
            return _currentFrequency;
        }
        return _radioFrequency;
    }

    void setFrequency(int frequency)
    {
        _drawable = null; /* TODO: ???*/
        _radioFrequency = frequency;
        _currentFrequency = 0;
        addFrequency(frequency);
        notifyChanged();
    }

    public List<Integer> getAlternativeFrequencies() {
        return alternativeFrequencies();
    }

    public boolean matches(int frequency) {
        return _radioFrequency == frequency || _alternativeFrequencies.contains(frequency);
    }

    public int getPi() {
        return _pi;
    }

    public void setPi(int pi) {
        if (pi != _pi) {
            _pi = pi;
            notifyChanged();
        }
    }

    public void addFrequency(int frequency) {
        if (!_alternativeFrequencies.contains(frequency)) {
            for (int i=0; i<_alternativeFrequencies.size(); i++) {
                if (_alternativeFrequencies.get(i) > frequency) {
                    _alternativeFrequencies.add(i, frequency);
                    return;
                }
            }

            _alternativeFrequencies.add(frequency);
        }
    }

    public ArrayList<Integer> alternativeFrequencies()
    {
        return _alternativeFrequencies; //TODO: Return copy?
    }

    void fix() {
        setUserDefinedImage(_imagePath);
        setUserDefinedName(getDisplayName());
    }

    boolean isFixed() {
        return (_userDefinedImage != null && !_userDefinedImage.isEmpty()) ||
                (_userDefinedName != null && !_userDefinedName.isEmpty());
    }

    public Bitmap getBitmap() {
        if (_bitmap != null)
            return _bitmap;


        int canvasWidth = getImage().getIntrinsicWidth();
        int canvasHeight = getImage().getIntrinsicHeight();
        int xpos = 0;
        int ypos = 0;


        if (canvasWidth > canvasHeight) {
            canvasHeight = canvasWidth;
            xpos = 0;
            ypos = (canvasHeight - getImage().getIntrinsicHeight()) / 2;
        }
        else if (canvasHeight > canvasWidth) {
            canvasWidth = canvasHeight;
            xpos = (canvasHeight - getImage().getIntrinsicWidth()) / 2;
            ypos = 0;
        }

        if(getImage().getIntrinsicWidth() <= 0 || getImage().getIntrinsicHeight() <= 0) {
            _bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            _bitmap = Bitmap.createBitmap(canvasWidth, canvasHeight, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(_bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0,0,canvas.getWidth(), canvas.getHeight(), paint);
        Rect bounds = getImage().copyBounds();
        getImage().setBounds(xpos, ypos, canvasWidth - xpos, canvasHeight - ypos);
        getImage().draw(canvas);
        getImage().setBounds(bounds);
        return _bitmap;
    }

    public Drawable getImage() {
        if (_drawable != null)
            return _drawable;


        if (_userDefinedImage != null && !_userDefinedImage.isEmpty()) {
            try {
                _drawable = Drawable.createFromPath(_userDefinedImage);
            }
            catch (Exception exc) {}
        }

        String filename = "";
        if (_name != null)
            filename  = _name.toLowerCase().trim().replace(" ", "") + ".png";
        //TODO: Schleife
        if (_drawable == null && _name != null && !_name.isEmpty()) {
            try {
                File file = new File(Constants.LOGO_STORAGE_PATH + filename);

                if (file.exists()) {
                    _drawable = Drawable.createFromPath(Constants.LOGO_STORAGE_PATH + filename);
                    _imagePath = Constants.LOGO_STORAGE_PATH + filename;
                    //TODO: save to internal card???
                }
            }
            catch (Exception exc) {}
        }

        if (_drawable == null)
        {
            _drawable = MainActivity._instance.getDrawable(R.drawable.station_default);
            _imagePath = "";
        }
        return _drawable;

    }

    public Boolean getAlternativeFrequenciesEnabled()
    {
        return _alternativeFrequencieEnabled;
    }

    public void setAlternativeFrequenciesEnabled(Boolean alternativeFrequency)
    {
        // TODO: Notify
        _alternativeFrequencieEnabled = alternativeFrequency;
    }

    public void setUserDefinedName(String userDefinedName) {
        _userDefinedName = userDefinedName;
        notifyChanged();
    }

    public void setUserDefinedImage(String pathToImage) {
        _userDefinedImage = pathToImage;
        _drawable = null;
        _bitmap = null;
        notifyChanged();
    }

    public String getUserDefinedImage() {
        return _userDefinedImage;
    }
    public void resetImage() {
        _drawable = null;
        _bitmap = null;
        notifyChanged();
    }

    public void clearAlternatives() {
        _alternativeFrequencies.clear();
        _alternativeFrequencies.add(_radioFrequency);
        notifyChanged();
    }
}
